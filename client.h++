#pragma once

namespace server {
enum Server {
    // Plugin calls
    PluginCall = 0x0001,
    // Game managements
    CreateGame = 0x0101,
    UpdateGame,
    CloneGame,
    // Plugin managements
    CreatePlugin = 0x0201,
    CreateVersion,
    ListPluginFiles,
    UploadPluginFile,
    DownlaodPluginFile,
    // Plugins and games management
    AddPluginToGame = 0x0301,
    RemovePluginFromGame,
    // Users and games management
    AddUserToGame = 0x0401,
    RemoveUserFromGame,
    LeaveGame,
    // Login functions
    Login = 0x0501,
    Register
};
}
