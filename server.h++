#pragma once

namespace client {

enum Client {
    // Plugin calls
    PluginCall = 0x0001,
    // Game managements
    GameCreated = 0x0101,
    GameUpdated,
    GameCloned,
    // Plugin managements
    PluginCreated = 0x0201,
    VersionCreated,
    ListPluginFiles,
    PluginFileUpdated,
    SavePluginFile,
    // Plugins and games management
    PluginAddedToGame = 0x0301,
    PluginRemovedFromGame,
    // Users and games management
    UserAddedToGame = 0x0401,
    UserRemovedFromGame,
    // Login functions
    LoginSuccesful = 0x0501,
    RegisterSuccessful,
    // Error
    Error = 0xFFFF
};

}
